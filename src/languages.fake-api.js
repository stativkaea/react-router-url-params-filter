const languages = [
  {
    name: 'JavaScript',
    compiled: false,
    typed: false,
  },
  {
    name: 'Java',
    compiled: true,
    typed: true,
  },
  {
    name: 'C#',
    compiled: true,
    typed: true,
  },
  {
    name: 'Swift',
    compiled: true,
    typed: true,
  },
  {
    name: 'Python',
    compiled: false,
    typed: true,
  },
]

export const fetchLanguages = (...filters) =>
  Promise.resolve(
    languages.filter(lang => {
      return filters && filters.length
        ? filters.every(filter => lang[filter])
        : true
    }),
  )
