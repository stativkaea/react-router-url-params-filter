import React from 'react'
import { connect } from 'react-redux'
import { WithSearchParams } from './WithSearchParams'

const Languages = ({ languages = [] }) => (
  <ul>
    {languages.map(language => <li key={language.name}>{language.name}</li>)}
  </ul>
)

const mapStateToProps = (state, ownProps) => ({
  languages: state.languages,
})

export default connect(mapStateToProps)(Languages)
