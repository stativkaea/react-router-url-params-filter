import { call, put, takeLatest } from 'redux-saga/effects'
import * as api from './languages.fake-api'
import {
  fetchLanguagesSuccess,
  fetchLanguagesFail,
  FETCH_LANGUAGES,
} from './languages.actions'

function* fetchData(action) {
  try {
    const languages = yield call(api.fetchLanguages, ...action.payload)
    yield put(fetchLanguagesSuccess(languages))
  } catch (e) {
    yield put(fetchLanguagesFail(e.message))
  }
}

function* mySaga() {
  yield takeLatest(FETCH_LANGUAGES, fetchData)
}

export default mySaga
