import React from 'react'
import { NavLink, withRouter } from 'react-router-dom'
import { compose } from 'recompose'
import { parse } from 'query-string'
import { WithSearchParams } from './WithSearchParams'

const isActive = filter => (match, location) => {
  const { search } = location
  const searchParams = parse(search)

  console.log('locationt = ', !location.search)
  console.log('filter = ', !filter)

  return filter
    ? Object.prototype.hasOwnProperty.call(searchParams, filter)
    : !location.search
}

const isTypedActive = isActive('typed')
const isCompiledActive = isActive('compiled')
const isAllActive = isActive()

const styles = { marginRight: '15px' }

const Filters = ({ location: { search }, searchParams }) => [
  <NavLink
    isActive={isAllActive}
    style={!search ? { ...styles, color: 'red' } : styles}
    key="all"
    to="/"
  >
    All languages
  </NavLink>,
  <NavLink
    isActive={isTypedActive}
    style={
      Object.prototype.hasOwnProperty.call(searchParams, 'typed') ? (
        { ...styles, color: 'red' }
      ) : (
        styles
      )
    }
    key="typed"
    to={{
      pathname: '/',
      search: '?typed',
    }}
  >
    Typed
  </NavLink>,
  <NavLink
    isActive={isCompiledActive}
    style={
      Object.prototype.hasOwnProperty.call(searchParams, 'compiled') ? (
        {
          ...styles,
          color: 'red',
        }
      ) : (
        {}
      )
    }
    key="compiled"
    to={{
      pathname: '/',
      search: '?compiled',
    }}
  >
    Compiled
  </NavLink>,
]

const enhance = compose(withRouter, WithSearchParams)

export default enhance(Filters)
