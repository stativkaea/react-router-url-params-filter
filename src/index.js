import React from 'react'
import { render } from 'react-dom'
import { getStore } from './store'
import { Root } from './Root'

const store = getStore()

render(<Root store={store} />, document.getElementById('root'))
