import { FETCH_LANGUAGES_SUCCESS } from './languages.actions'

export const languages = (state = { languages: [] }, action) => {
  switch (action.type) {
    case FETCH_LANGUAGES_SUCCESS:
      return { languages: action.payload }
    default:
      return state
  }
}
