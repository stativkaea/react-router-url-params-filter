import React from 'react'
import { parse } from 'query-string'

export const WithSearchParams = Comp => props => {
  const { location: { search } } = props

  const propsWithSearchParams = { ...props, searchParams: parse(search) }

  return <Comp {...propsWithSearchParams} />
}
