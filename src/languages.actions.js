export const FETCH_LANGUAGES = 'FETCH_LANGUAGES'
export const FETCH_LANGUAGES_SUCCESS = 'FETCH_LANGUAGES_SUCCESS'
export const FETCH_LANGUAGES_FAIL = 'FETCH_LANGUAGES_FAIL'

export const fetchLanguages = (...filters) => ({
  type: FETCH_LANGUAGES,
  payload: filters || [],
})

export const fetchLanguagesSuccess = languages => ({
  type: FETCH_LANGUAGES_SUCCESS,
  payload: languages,
})

export const fetchLanguagesFail = error => ({
  type: FETCH_LANGUAGES_FAIL,
  payload: error,
})
