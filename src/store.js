import { applyMiddleware, createStore } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension/logOnlyInProduction'
import createSagaMiddleware from 'redux-saga'
import { languages } from './languages.reducer'
import mySaga from './languages.saga'

export const getStore = () => {
  const sagaMiddleware = createSagaMiddleware()

  const store = createStore(
    languages,
    composeWithDevTools(applyMiddleware(sagaMiddleware)),
  )

  sagaMiddleware.run(mySaga)

  return store
}
