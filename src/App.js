import React from 'react'
import { connect } from 'react-redux'
import { lifecycle, compose } from 'recompose'
import Languages from './Languages'
import Filters from './Filters'
import { fetchLanguages } from './languages.actions'
import { WithSearchParams } from './WithSearchParams'

const App = () => (
  <main>
    <Filters />
    <Languages />
  </main>
)

const mapDispatchToProps = dispatch => ({
  fetchLanguages(...filters) {
    dispatch(fetchLanguages(...filters))
  },
})

const enhance = compose(
  connect(null, mapDispatchToProps),
  WithSearchParams,
  lifecycle({
    componentDidMount() {
      this.props.fetchLanguages(...Object.keys(this.props.searchParams))
    },
    componentWillReceiveProps(nextProps) {
      if (this.props.location.search !== nextProps.location.search) {
        this.props.fetchLanguages(...Object.keys(nextProps.searchParams))
      }
    },
  }),
)

export default enhance(App)
